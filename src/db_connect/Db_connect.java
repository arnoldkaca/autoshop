package db_connect;

import java.sql.Connection;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

public class Db_connect {

        //information to connect with mysql database
    public String url = "jdbc:mysql://localhost:3306/autoshop";
    public String UID = "root";
    public String password = "";
    
    
    public static int USER_ID;
    public static String USER_FNAME;
    public static String USER_LNAME;
    public static String USERNAME;
    
    private Connection connection;
    public void getConnected(){
        try {
            setConnection(DriverManager.getConnection(url, UID, password));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Couldn't connect to the database.");
        }
    }
    
    public Connection getConnection(){
        if(connection==null){
            getConnected();
        }
        return connection;
    }
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
