package com.org.controllers;
import com.org.models.LoginModel;
import javax.swing.JOptionPane;
public class LoginController {
    public void login(String username, String password){
        LoginModel lm = new LoginModel();
        lm.login(username, password);
    }
    public void validateandLogin(String username, String password){
        if(username.isEmpty() || password.isEmpty()){
            JOptionPane.showMessageDialog(null, "You should enter the username and password.");
        } else {
            login(username, password);
        } }
    public void loginbyTicket(int ticket) {
        LoginModel lm = new LoginModel();
        lm.loginByTicket(ticket);
    }
}
