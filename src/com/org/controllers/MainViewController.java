package com.org.controllers;

import com.org.models.MainViewModel;
import db_connect.Db_connect;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JTable;

public class MainViewController {
    Db_connect db = new Db_connect();
    MainViewModel mm = new MainViewModel();
    public void shownComponents(JLabel username, JLabel user_id, JLabel name, JLabel surname, JLabel current_time, JTable jTable1) {
        username.setText(db.USERNAME);
        user_id.setText(String.valueOf(db.USER_ID));
        name.setText(db.USER_FNAME);
        surname.setText(db.USER_LNAME);
        showTime(current_time);
        mm.showTableDetails(jTable1);
        
        
    }
    
    private void showTime(JLabel c_t){
        String current_time = "";
        Thread thr = new Thread(){
            @Override
            public void run() {
                SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                while(true){
                    Date dt = new Date();
                    c_t.setText(df.format(dt));
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) { System.out.println("Thread couldn't sleep."); }
                }
                
            }
        };
        thr.start();
    }

    public void releaseCar(JTable jTable1) {
        MainViewModel mvd = new MainViewModel();
        mvd.releaseCar(jTable1);
    }
}
