package com.org.controllers;

import com.org.models.SignupModel;
import javax.swing.JOptionPane;

public class SignupController {
    public void validateandSignup(String first_name, String last_name, String username, String password, String email, String mobile_no, String address){
        String first_name_pattern = "[A-Za-z]{1,50}";
        String last_name_pattern = "[A-Za-z]{1,50}";
        String username_pattern = "[A-Za-z0-9._-]{1,50}";
        String email_pattern = "(?=.*[@#$%^&+=()\\[\\]]).{8,}";
        String mobile_no_pattern = "[0-9]{14}";
        String address_pattern = "[a-zA-Z0-9\\s]+";
        
        if(!first_name.matches(first_name_pattern)){
            JOptionPane.showMessageDialog(null, "First name contains letters only.");
            return;
        }
        if(!last_name.matches(last_name_pattern)){
            JOptionPane.showMessageDialog(null, "Last name contains letters only.");
            return;
        }
        if(!username.matches(username_pattern)){
            JOptionPane.showMessageDialog(null, "Username contains letters and numbers only.");
            return;
        }
        if(!email.matches(email_pattern)){
            JOptionPane.showMessageDialog(null, "Email doesn't match.");
            return;
        }
        if(!mobile_no.matches(mobile_no_pattern)){
            JOptionPane.showMessageDialog(null, "Mobile number doesn't match.");
            return;
        }
        if(!address.matches(address_pattern)){
            JOptionPane.showMessageDialog(null, "Address is wrong.");
            return;
        }
        
        SignupModel sm = new SignupModel();
        sm.validateandSignup(first_name, last_name, username, password, email, mobile_no, address);
    }
}
