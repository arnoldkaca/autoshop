package com.org.controllers;

import com.org.models.NewCarModel;
import java.sql.Timestamp;
import javax.swing.JOptionPane;

public class NewCarController {

    public void validateandCreatenewCar(String first_name, String last_name, String phone_number, String address, String car_model, int price, String problem_type, Timestamp date) {
        if(validate()){
            NewCarModel ncm = new NewCarModel();
            ncm.createnewCar(first_name, last_name, phone_number, address, car_model, price, problem_type, date);
        } else { JOptionPane.showMessageDialog(null, "Message here");}
        
        
        
    }

    private boolean validate() {
        
        //remove this to validate 
        return true;
    }
    
}
