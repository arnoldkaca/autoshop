package com.org.models;

import db_connect.Db_connect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;

public class checkCarModel {
    Db_connect db =  new Db_connect();
    public void fillDatas(JLabel name, JLabel surname, JLabel phone, JLabel address, JLabel car, JLabel problem, JLabel chkin, JLabel release, JLabel price, int id) {
        try {
            //                    1      2       3       4         5        6           7             8           9   
            String sql = "select name,surname,phone_nu,address,car_model,problem_type,updated_at,to_be_finished,price from cars where id=?";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setInt(1, id);
            ResultSet rs=pst.executeQuery();
            SimpleDateFormat dtf = new SimpleDateFormat();
            Date dt = new Date();
            
            if(rs.next()){
                name.setText(rs.getString(1));
                surname.setText(rs.getString(2));
                phone.setText(rs.getString(3));
                address.setText(rs.getString(4));
                car.setText(rs.getString(5));
                problem.setText(rs.getString(6));
                
                Timestamp tm = rs.getTimestamp(7);
                    dt.setYear(tm.getYear());
                    dt.setMonth(tm.getMonth());
                    dt.setDate(tm.getDate());
                    dt.setHours(tm.getHours());
                    dt.setMinutes(tm.getMinutes());
                    dt.setSeconds(tm.getSeconds());
                chkin.setText(dtf.format(dt));
                tm = rs.getTimestamp(8);
                    dt.setYear(tm.getYear());
                    dt.setMonth(tm.getMonth());
                    dt.setDate(tm.getDate());
                    dt.setHours(tm.getHours());
                    dt.setMinutes(tm.getMinutes());
                    dt.setSeconds(tm.getSeconds());
                release.setText(dtf.format(dt));
                price.setText(String.valueOf(rs.getInt(9)));
                
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    
}
