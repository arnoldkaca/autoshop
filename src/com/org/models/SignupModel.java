package com.org.models;


import db_connect.Db_connect;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;

public class SignupModel {
    Db_connect db = new Db_connect();
    public void validateandSignup(String first_name, String last_name, String username, String password, String email, String mobile_no, String address){
        try{
            String sql = "insert into members (email, address, mobile_no, first_name, last_name, status, updated_at, updated_by)";
            sql += "values(?, ?, ?, ?, ?, 1, current_timestamp, "+db.USER_ID+");";
            sql += "insert into users (username, password)";
            sql += "values(?,?)";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setString(1, email);
            pst.setString(2, address);
            pst.setString(3, mobile_no);
            pst.setString(4, first_name);
            pst.setString(5, last_name);
            pst.setString(6, username);
            pst.setString(7, password);
            pst.executeUpdate();
            JOptionPane.showMessageDialog(null, "You have successfully registered.");
                    
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Current user exists.");
        }
    }
}
