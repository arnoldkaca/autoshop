package com.org.models;

import com.org.views.Login;
import db_connect.Db_connect;
import com.org.views.checkCar;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class LoginModel {
    Db_connect db = new Db_connect();
    public void login(String username, String password){
        try {
            String sql = "SELECT * FROM users WHERE username=? and password=?";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setString(1, username);
            pst.setString(2, password);
            
            ResultSet rs;
            rs = pst.executeQuery();
            if(rs.next()){
                db.USER_ID = rs.getInt(1);
                db.USERNAME = rs.getString(2);
                try{
                    String sql1 = "select * from members where member_id=?";
                    PreparedStatement pst1 = db.getConnection().prepareStatement(sql1);
                    pst1.setInt(1, db.USER_ID);
                    ResultSet rs1;
                    rs1 = pst1.executeQuery();
                    rs1.next();
                    db.USER_FNAME = rs1.getString("first_name");
                    db.USER_LNAME = rs1.getString("last_name");
                } catch(Exception err){
                
                }
            } else {
                JOptionPane.showMessageDialog(null, "Username and pasword not found in the database");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ann error with the database occurred");
        }
    }

    public void loginByTicket(int ticket) {
        try {
            String sql = "SELECT id FROM cars WHERE id=? and status=1";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setInt(1, ticket);
            
            ResultSet rs;
            rs = pst.executeQuery();
            if(rs.next()){
                checkCar chk = new checkCar(ticket);
                chk.setVisible(true);
            } else {
                JOptionPane.showMessageDialog(null, "Ticket number odesn't exist or is expired.");
                Login lg = new Login();
                lg.setVisible(true);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Ann error with the database occurred");
        }
    }
}
