package com.org.models;

import db_connect.Db_connect;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

public class NewCarModel {
    
    Db_connect db = new Db_connect();
    
    public void createnewCar(String first_name, String last_name, String phone_number, String address, String car_model, int price, String problem_type, Timestamp date) {
        
            
            
        try {
            String sql = "  INSERT INTO cars (name,surname,phone_nu,address,car_model, problem_type, to_be_finished, status, price, updated_at, updated_by)\n" +
                            "VALUES (           ?,      ?,      ?,      ?,      ?,          ?,              ?,          1,      ? , current_timestamp, ?);";
            Timestamp start = new Timestamp(date.getYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), 00);
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setString(1, first_name);
            pst.setString(2, last_name);
            pst.setString(3, phone_number);
            pst.setString(4, address);
            pst.setString(5, car_model);
            pst.setString(6, problem_type);
            pst.setTimestamp(7, date);
            pst.setInt(8, price);
            pst.setInt(9, db.USER_ID);
            pst.executeUpdate();
            System.out.println("Car has been added.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
            
        
        
    }
    
}
