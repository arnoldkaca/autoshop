package com.org.models;

import db_connect.Db_connect;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class MainViewModel {
    Db_connect db = new Db_connect();

    public void showTableDetails(JTable jTable1) {
        int total_data = 0;
        try {
            String sql = "select count(name) from cars where status=1";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            rs.next();
            total_data = rs.getInt(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        String[] names = new String[total_data];
        String[] surnames = new String[total_data];
        String[] phone_numbers = new String[total_data];
        String[] addresses = new String[total_data];
        String[] car_models = new String[total_data];
        String[] problem_types = new String[total_data];
        Date[] addedd = new Date[total_data];
        Date[] to_be_done = new Date[total_data];
        int[] prices = new int[total_data];
        int[] ids = new int[total_data];
        
        
        try {
            //name surname phone_number address car_model addedd Tobefinished price
            //                  1       2           3       4       5       6           7           8             9   10
            String sql = "select name,surname,phone_nu,address,car_model,problem_type,updated_at,to_be_finished,price,id from cars where status=1 order by name asc";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            int i=0;
            
            while(rs.next()){
                addedd[i] = new Date();
                to_be_done[i] = new Date();
                names[i] = rs.getString(1);
                surnames[i] = rs.getString(2);
                phone_numbers[i] = rs.getString(3);
                addresses[i] = rs.getString(4);
                car_models[i] = rs.getString(5);
                problem_types[i] = rs.getString(6);
                Timestamp tm = rs.getTimestamp(7);
                    addedd[i].setYear(tm.getYear());
                    addedd[i].setMonth(tm.getMonth());
                    addedd[i].setDate(tm.getDate());
                    addedd[i].setHours(tm.getHours());
                    addedd[i].setMinutes(tm.getMinutes());
                    addedd[i].setSeconds(tm.getSeconds());
                tm = rs.getTimestamp(8);
                    to_be_done[i].setYear(tm.getYear());
                    to_be_done[i].setMonth(tm.getMonth());
                    to_be_done[i].setDate(tm.getDate());
                    to_be_done[i].setHours(tm.getHours());
                    to_be_done[i].setMinutes(tm.getMinutes());
                    to_be_done[i].setSeconds(tm.getSeconds());
                prices[i] = rs.getInt(9);
                ids[i] = rs.getInt(10);
                i++;
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        SimpleDateFormat dtf = new SimpleDateFormat("dd-MM-YY hh:mm");
        DefaultTableModel df = (DefaultTableModel)jTable1.getModel();
        df.setRowCount(total_data);
        for(int i=0;i<10;i++){
            for(int j=0;j<names.length;j++){
                switch(i){
                    case 0: df.setValueAt(names[j], j, i);
                        break;
                    case 1: df.setValueAt(surnames[j], j, i);
                        break;
                    case 2: df.setValueAt(phone_numbers[j], j, i);
                        break;
                    case 3: df.setValueAt(addresses[j], j, i);
                        break;
                    case 4: df.setValueAt(car_models[j], j, i);
                        break;
                    case 5: df.setValueAt(problem_types[j], j, i);
                        break;
                    case 6: df.setValueAt(dtf.format(addedd[j]), j, i);
                        break;  
                    case 7: df.setValueAt(dtf.format(to_be_done[j]), j, i);
                        break;  
                    case 8: df.setValueAt(prices[j], j, i);
                        break;  
                    case 9: df.setValueAt(ids[j], j, i);
                        break; 
                }
            }
        }

        jTable1.setModel(df);
        
    }

    public void releaseCar(JTable jTable1) {
        MainViewModel mvd = new MainViewModel();
        int id= Integer.parseInt(String.valueOf(jTable1.getModel().getValueAt(jTable1.getSelectedRow(), 9)));
        System.out.println("id is: "+id);
        try {
            String sql = "update cars set status=0 where id=?";
            PreparedStatement pst = db.getConnection().prepareStatement(sql);
            pst.setInt(1, id);
            pst.executeUpdate();
            this.showTableDetails(jTable1);
            JOptionPane.showMessageDialog(null, "Car has been released.");
        } catch (SQLException e) {
        }
    }
    
}
