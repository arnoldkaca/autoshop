# Auto Shop
Java desktop application used to manage car queues in a car service  business. 
# Features!

  - Login used for different employees in the business
  - Adding Car in the queue with the corresponding information about the car and the client.
  - Viewing current cars and which car is next in the queue.
  - Checking if the car is finished by a client.
  - Finishing and paying the price for the service.


### Tech

* Java
* MySQL
* MVC design Pattern


### Installation

Install the database from database folder

Run this project in a JAVA IDE

Use JDBC driver if it is not connecting

then it is ready to use