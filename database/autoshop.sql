-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 26, 2017 at 01:13 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `autoshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `phone_nu` varchar(20) NOT NULL,
  `address` varchar(50) NOT NULL,
  `car_model` varchar(50) NOT NULL,
  `problem_type` varchar(35) NOT NULL,
  `to_be_finished` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  `price` int(20) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `name`, `surname`, `phone_nu`, `address`, `car_model`, `problem_type`, `to_be_finished`, `status`, `price`, `updated_at`, `updated_by`) VALUES
(1, 'asdsad', 'asdasd', 'asd', 'asdsad', 'asdasd', 'asdasd', '2017-02-25 03:30:00', 0, 123, '2017-02-25 23:28:17', 0),
(2, 'asdasd', 'asdsadas', 'asdsad', 'asdasd', 'asdasd', 'zxczxc', '2017-04-15 04:33:00', 0, 123, '2017-02-25 23:28:13', 0),
(3, 'Arnold', 'Kaca', '2332', '132321123423sdfs', 'BMW', 'Engine', '2017-03-04 17:50:00', 1, 250, '2017-02-25 10:44:05', 0),
(4, 'aasdasd', 'asdasd', 'asd', 'asdasd', 'Lambo', 'zxc engine', '2017-02-25 15:15:00', 1, 123456, '2017-02-25 10:45:40', 0),
(5, 'Dorian', 'Shatku', '0682331253', 'rruga Dibra', 'Golf 1', 'Rrotat e parme', '2017-02-27 03:29:00', 0, 200, '2017-02-25 23:28:07', 1),
(6, 'Fabian', 'Lashi', '0642565321', 'rruga Legjendaret', 'Ferrari', 'none', '2017-02-27 11:06:00', 1, 70000, '2017-02-25 23:02:38', 1),
(7, 'Petraq', 'Papajorgji', '0900750456231', 'rruga Deshmoret e Kombit', 'iCar', 'Wirless Card', '2017-02-26 13:01:00', 1, 850, '2017-02-26 00:11:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `member_id` int(8) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(55) NOT NULL,
  `mobile_no` varchar(16) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_by` int(8) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`member_id`, `email`, `address`, `mobile_no`, `first_name`, `last_name`, `status`, `updated_at`, `updated_by`) VALUES
(1, 'dorianshatku@gmail.com', 'rruga Dibres', '00355694545455', 'Dorian', 'Shatku', 1, '2017-02-20 13:27:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` int(2) NOT NULL,
  `status` int(1) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `role`, `status`, `updated_at`) VALUES
(1, 'a', 'a', 1, 1, '2017-02-20 12:04:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`member_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `member_id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
