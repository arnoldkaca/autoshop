/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agileJunit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Merita
 */
public class MyAgileTest2Test {
    
    public MyAgileTest2Test() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class MyAgileTest2.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        String ticket1 = "2";
        String ticket2 = "3";
        MyAgileTest2 instance = new MyAgileTest2();
        int expResult = 5;
        int result = instance.add(ticket1, ticket2);
        assertEquals(expResult, result);
        
    }
    
}
