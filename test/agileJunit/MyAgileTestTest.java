/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agileJunit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author Merita
 */
public class MyAgileTestTest {
    
    public MyAgileTestTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSomeMethod() {
        
    }

    /**
     * Test of add method, of class MyAgileTest.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        String ticket1 = "2";
        String ticket2= "3";
        MyAgileTest instance = new MyAgileTest();
        int expResult = 5;
        int result = instance.add(ticket1, ticket2);
        assertEquals(expResult, result);
        
    }
    
}
