/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.org.controllers;

import java.sql.Timestamp;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Merita
 */
public class NewCarControllerTest {
    
    public NewCarControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of validateandCreatenewCar method, of class NewCarController.
     */
    @Test
    public void testValidateandCreatenewCar() {
        System.out.println("validateandCreatenewCar");
        String first_name = "Merita";
        String last_name = "Qorllari";
        String phone_number = "0698866555";
        String address = "Tirana";
        String car_model = "BMW";
        int price = 5;
        String problem_type = "XHAMAT";
        Timestamp date = null;
        NewCarController instance = new NewCarController();
        instance.validateandCreatenewCar(first_name, last_name, phone_number, address, car_model, price, problem_type, date);
        
    }
    
}
